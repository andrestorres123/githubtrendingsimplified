package com.andrestorres.githubtrendingsimplified.mobile.injection

import android.app.Application
import com.andrestorres.githubtrendingsimplified.cache.db.ProjectsDatabase
import com.andrestorres.githubtrendingsimplified.data.repository.ProjectsCache
import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides

@Module
object TestCacheModule {

    @Provides
    @JvmStatic
    fun provideDatabase(application: Application): ProjectsDatabase {
        return ProjectsDatabase.getInstance(application)
    }

    @Provides
    @JvmStatic
    fun provideProjectsCache(): ProjectsCache {
        return mock()
    }

}