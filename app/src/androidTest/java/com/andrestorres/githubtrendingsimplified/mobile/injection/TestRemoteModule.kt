package com.andrestorres.githubtrendingsimplified.mobile.injection

import com.andrestorres.githubtrendingsimplified.data.repository.ProjectsRemote
import com.andrestorres.githubtrendingsimplified.remote.service.GithubTrendingService
import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides

@Module
object TestRemoteModule {

    @Provides
    @JvmStatic
    fun provideGithubService(): GithubTrendingService {
        return mock()
    }

    @Provides
    @JvmStatic
    fun provideProjectsRemote(): ProjectsRemote {
        return mock()
    }

}