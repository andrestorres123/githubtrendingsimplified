package com.andrestorres.githubtrendingsimplified.mobile.injection

import android.app.Application
import com.andrestorres.githubtrendingsimplified.domain.repository.ProjectsRepository
import com.andrestorres.githubtrendingsimplified.mobile.injection.module.PresentationModule
import com.andrestorres.githubtrendingsimplified.mobile.injection.module.UiModule
import com.andrestorres.githubtrendingsimplified.mobile.test.TestApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidSupportInjectionModule::class,
        TestApplicationModule::class,
        TestCacheModule::class,
        TestDataModule::class,
        PresentationModule::class,
        UiModule::class,
        TestRemoteModule::class))
interface TestApplicationComponent {

    fun projectsRepository(): ProjectsRepository

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): TestApplicationComponent
    }

    fun inject(application: TestApplication)

}