package com.andrestorres.githubtrendingsimplified.remote

import com.andrestorres.githubtrendingsimplified.data.repository.ProjectsRemote
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import com.andrestorres.githubtrendingsimplified.remote.service.GithubTrendingService
import io.reactivex.Flowable
import javax.inject.Inject

class ProjectsRemoteImpl @Inject constructor(private val service: GithubTrendingService) : ProjectsRemote {

    override fun getProjects(): Flowable<List<ProjectModel>> {
        return service.searchRepositories("language:kotlin", "stars", "desc")
                .map {
                    it.items
                }
    }
}