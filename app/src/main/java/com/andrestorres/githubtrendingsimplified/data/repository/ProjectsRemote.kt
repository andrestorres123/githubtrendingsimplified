package com.andrestorres.githubtrendingsimplified.data.repository

import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import io.reactivex.Flowable

interface ProjectsRemote {

    fun getProjects(): Flowable<List<ProjectModel>>

}