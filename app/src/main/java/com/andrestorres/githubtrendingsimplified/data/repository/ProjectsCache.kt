package com.andrestorres.githubtrendingsimplified.data.repository

import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface ProjectsCache  {

    fun clearProjects(): Completable

    fun saveProjects(projects: List<ProjectModel>): Completable

    fun getProjects(): Flowable<List<ProjectModel>>

    fun getBookmarkedProjects(): Flowable<List<ProjectModel>>

    fun setProjectAsBookmarked(projectId: String): Completable

    fun setProjectAsNotBookmarked(projectId: String): Completable

    fun areProjectsCached(): Single<Boolean>

    fun setLastCacheTime(lastCache: Long): Completable

    fun isProjectsCacheExpired(): Single<Boolean>

}