package com.andrestorres.githubtrendingsimplified.data.repository

import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import io.reactivex.Completable
import io.reactivex.Flowable

interface ProjectsDataStore {

    fun getProjects(): Flowable<List<ProjectModel>>

    fun saveProjects(projects: List<ProjectModel>): Completable

    fun clearProjects(): Completable

    fun getBookmarkedProjects(): Flowable<List<ProjectModel>>

    fun setProjectAsBookmarked(projectId: String): Completable

    fun setProjectAsNotBookmarked(projectId: String): Completable

}