package com.andrestorres.githubtrendingsimplified.data.store

import com.andrestorres.githubtrendingsimplified.data.repository.ProjectsCache
import com.andrestorres.githubtrendingsimplified.data.repository.ProjectsDataStore
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

open class ProjectsCacheDataStore @Inject constructor(
        private val projectsCache: ProjectsCache)
    : ProjectsDataStore {

    override fun getProjects(): Flowable<List<ProjectModel>> {
        return projectsCache.getProjects()
    }

    override fun saveProjects(projects: List<ProjectModel>): Completable {
        return projectsCache.saveProjects(projects)
                .andThen(projectsCache.setLastCacheTime(System.currentTimeMillis()))
    }

    override fun clearProjects(): Completable {
        return projectsCache.clearProjects()
    }

    override fun getBookmarkedProjects(): Flowable<List<ProjectModel>> {
        return projectsCache.getBookmarkedProjects()
    }

    override fun setProjectAsBookmarked(projectId: String): Completable {
        return projectsCache.setProjectAsBookmarked(projectId)
    }

    override fun setProjectAsNotBookmarked(projectId: String): Completable {
        return projectsCache.setProjectAsNotBookmarked(projectId)
    }

}