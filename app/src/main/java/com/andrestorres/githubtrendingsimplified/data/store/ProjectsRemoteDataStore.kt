package com.andrestorres.githubtrendingsimplified.data.store

import com.andrestorres.githubtrendingsimplified.data.repository.ProjectsDataStore
import com.andrestorres.githubtrendingsimplified.data.repository.ProjectsRemote
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

open class ProjectsRemoteDataStore @Inject constructor(
        private val projectsRemote: ProjectsRemote)
    : ProjectsDataStore {

    override fun getProjects(): Flowable<List<ProjectModel>> {
        return projectsRemote.getProjects()
    }

    override fun saveProjects(projects: List<ProjectModel>): Completable {
        throw UnsupportedOperationException("Saving projects isn't supported here...")
    }

    override fun clearProjects(): Completable {
        throw UnsupportedOperationException("Clearing projects isn't supported here...")
    }

    override fun getBookmarkedProjects(): Flowable<List<ProjectModel>> {
        throw UnsupportedOperationException("Getting bookmarked projects isn't supported here...")
    }

    override fun setProjectAsBookmarked(projectId: String): Completable {
        throw UnsupportedOperationException("Setting bookmarks isn't supported here...")
    }

    override fun setProjectAsNotBookmarked(projectId: String): Completable {
        throw UnsupportedOperationException("Setting bookmarks isn't supported here...")
    }

}