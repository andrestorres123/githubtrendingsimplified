package com.andrestorres.githubtrendingsimplified.cache.mapper

import com.andrestorres.githubtrendingsimplified.cache.model.CachedProject
import com.andrestorres.githubtrendingsimplified.domain.model.OwnerModel
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import javax.inject.Inject

class CachedProjectMapper @Inject constructor() : CacheMapper<CachedProject, ProjectModel> {

    override fun mapFromCached(type: CachedProject): ProjectModel {
        val owner = OwnerModel(type.ownerName, type.ownerAvatar)
        return ProjectModel(type.id, type.name, type.fullName, type.starCount.toInt(),
                type.dateCreated, type.isBookmarked, owner)
    }

    override fun mapToCached(type: ProjectModel): CachedProject {
        return CachedProject(type.id, type.name, type.fullName, type.starCount.toString(),
                type.dateCreated, type.owner.ownerName, type.owner.ownerAvatar,
                type.isBookmarked)
    }

}