package com.andrestorres.githubtrendingsimplified.domain.model

class ProjectsResponseModel(val items: List<ProjectModel>)