package com.andrestorres.githubtrendingsimplified.domain.actions.browse

import com.andrestorres.githubtrendingsimplified.domain.usecases.ObservableUseCase
import com.andrestorres.githubtrendingsimplified.domain.executor.PostExecutionThread
import com.andrestorres.githubtrendingsimplified.domain.repository.ProjectsRepository
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import io.reactivex.Observable
import javax.inject.Inject

open class GetProjects @Inject constructor(
        private val projectsRepository: ProjectsRepository,
        postExecutionThread: PostExecutionThread)
    : ObservableUseCase<List<ProjectModel>, Nothing?>(postExecutionThread) {

    public override fun buildUseCaseObservable(params: Nothing?): Observable<List<ProjectModel>> {
        return projectsRepository.getProjects()
    }

}