package com.andrestorres.githubtrendingsimplified.domain.repository

import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import io.reactivex.Completable
import io.reactivex.Observable

interface ProjectsRepository {

    fun getProjects(): Observable<List<ProjectModel>>

    fun bookmarkProject(projectId: String): Completable

    fun unbookmarkProject(projectId: String): Completable

    fun getBookmarkedProjects(): Observable<List<ProjectModel>>

}