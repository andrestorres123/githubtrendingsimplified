package com.andrestorres.githubtrendingsimplified.mobile.screens.browse

interface ProjectListener {

    fun onBookmarkedProjectClicked(projectId: String)

    fun onProjectClicked(projectId: String)

}