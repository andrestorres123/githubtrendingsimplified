package com.andrestorres.githubtrendingsimplified.mobile.screens.browse

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.andrestorres.githubtrendingsimplified.domain.actions.bookmark.BookmarkProject
import com.andrestorres.githubtrendingsimplified.domain.actions.bookmark.UnbookmarkProject
import com.andrestorres.githubtrendingsimplified.domain.actions.browse.GetProjects
import com.andrestorres.githubtrendingsimplified.mobile.state.Resource
import com.andrestorres.githubtrendingsimplified.mobile.state.ResourceState
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

open class BrowseProjectsViewModel @Inject internal constructor(
        private val getProjects: GetProjects?,
        private val bookmarkProject: BookmarkProject,
        private val unBookmarkProject: UnbookmarkProject): ViewModel() {

    private val liveData: MutableLiveData<Resource<List<ProjectModel>>> = MutableLiveData()

    init {
        fetchProjects()
    }

    override fun onCleared() {
        getProjects?.dispose()
        super.onCleared()
    }

    fun getProjects(): LiveData<Resource<List<ProjectModel>>> {
        return liveData
    }

    fun fetchProjects() {
        liveData.postValue(Resource(ResourceState.LOADING, null, null))
        getProjects?.execute(ProjectsSubscriber())
    }

    fun bookmarkProject(projectId: String) {
        return bookmarkProject.execute(BookmarkProjectsSubscriber(),
                BookmarkProject.Params.forProject(projectId))
    }

    fun unbookmarkProject(projectId: String) {
        return unBookmarkProject.execute(BookmarkProjectsSubscriber(),
                UnbookmarkProject.Params.forProject(projectId))
    }

    inner class ProjectsSubscriber: DisposableObserver<List<ProjectModel>>() {
        override fun onNext(t: List<ProjectModel>) {
            liveData.postValue(Resource(ResourceState.SUCCESS,
                    t, null))
        }

        override fun onComplete() { }

        override fun onError(e: Throwable) {
            liveData.postValue(Resource(ResourceState.ERROR, null, e.localizedMessage))
        }

    }

    inner class BookmarkProjectsSubscriber: DisposableCompletableObserver() {
        override fun onComplete() {
            liveData.postValue(Resource(ResourceState.SUCCESS, liveData.value?.data, null))
        }

        override fun onError(e: Throwable) {
            liveData.postValue(Resource(ResourceState.ERROR, liveData.value?.data,
                    e.localizedMessage))
        }

    }
}