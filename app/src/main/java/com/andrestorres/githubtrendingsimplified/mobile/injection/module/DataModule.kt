package com.andrestorres.githubtrendingsimplified.mobile.injection.module

import com.andrestorres.githubtrendingsimplified.data.ProjectsDataRepository
import com.andrestorres.githubtrendingsimplified.domain.repository.ProjectsRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {

    @Binds
    abstract fun bindDataRepository(dataRepository: ProjectsDataRepository): ProjectsRepository
}