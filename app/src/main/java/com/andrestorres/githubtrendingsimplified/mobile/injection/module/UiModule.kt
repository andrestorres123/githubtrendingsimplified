package com.andrestorres.githubtrendingsimplified.mobile.injection.module

import com.andrestorres.githubtrendingsimplified.domain.executor.PostExecutionThread
import com.andrestorres.githubtrendingsimplified.mobile.base.UiThread
import com.andrestorres.githubtrendingsimplified.mobile.screens.bookmarked.BookmarkedActivity
import com.andrestorres.githubtrendingsimplified.mobile.screens.browse.BrowseActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UiModule {

    @Binds
    abstract fun bindPostExecutionThread(uiThread: UiThread): PostExecutionThread

    @ContributesAndroidInjector
    abstract fun contributesBrowseActivity(): BrowseActivity

    @ContributesAndroidInjector
    abstract fun contributesBookmarkedActivity(): BookmarkedActivity
}