package com.andrestorres.githubtrendingsimplified.mobile.injection.module

import android.app.Application
import com.andrestorres.githubtrendingsimplified.cache.ProjectsCacheImpl
import com.andrestorres.githubtrendingsimplified.cache.db.ProjectsDatabase
import com.andrestorres.githubtrendingsimplified.data.repository.ProjectsCache
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class CacheModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun providesDataBase(application: Application): ProjectsDatabase {
            return ProjectsDatabase.getInstance(application)
        }
    }

    @Binds
    abstract fun bindProjectsCache(projectsCache: ProjectsCacheImpl): ProjectsCache
}