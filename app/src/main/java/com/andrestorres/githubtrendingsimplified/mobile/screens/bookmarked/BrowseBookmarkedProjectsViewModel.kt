package com.andrestorres.githubtrendingsimplified.mobile.screens.bookmarked

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.andrestorres.githubtrendingsimplified.domain.actions.bookmark.GetBookmarkedProjects
import com.andrestorres.githubtrendingsimplified.mobile.state.Resource
import com.andrestorres.githubtrendingsimplified.mobile.state.ResourceState
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class BrowseBookmarkedProjectsViewModel @Inject constructor(
        private val getBookmarkedProjects: GetBookmarkedProjects): ViewModel() {

    private val liveData: MutableLiveData<Resource<List<ProjectModel>>> =
            MutableLiveData()

    override fun onCleared() {
        getBookmarkedProjects.dispose()
        super.onCleared()
    }

    fun getProjects(): LiveData<Resource<List<ProjectModel>>> {
        return liveData
    }

    fun fetchProjects() {
        liveData.postValue(Resource(ResourceState.LOADING, null, null))
        return getBookmarkedProjects.execute(ProjectsSubscriber())
    }

    inner class ProjectsSubscriber: DisposableObserver<List<ProjectModel>>() {
        override fun onNext(t: List<ProjectModel>) {
            liveData.postValue(Resource(ResourceState.SUCCESS,
                    t, null))
        }

        override fun onError(e: Throwable) {
            liveData.postValue(Resource(ResourceState.ERROR, null,
                    e.localizedMessage))
        }

        override fun onComplete() { }

    }

}