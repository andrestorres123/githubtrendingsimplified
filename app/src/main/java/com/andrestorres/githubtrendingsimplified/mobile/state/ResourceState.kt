package com.andrestorres.githubtrendingsimplified.mobile.state

enum class ResourceState {
    LOADING, SUCCESS, ERROR
}