package com.andrestorres.githubtrendingsimplified.mobile.injection

import android.app.Application
import com.andrestorres.githubtrendingsimplified.mobile.base.GithubTrendingApplication
import com.andrestorres.githubtrendingsimplified.mobile.injection.module.ApplicationModule
import com.andrestorres.githubtrendingsimplified.mobile.injection.module.CacheModule
import com.andrestorres.githubtrendingsimplified.mobile.injection.module.DataModule
import com.andrestorres.githubtrendingsimplified.mobile.injection.module.PresentationModule
import com.andrestorres.githubtrendingsimplified.mobile.injection.module.RemoteModule
import com.andrestorres.githubtrendingsimplified.mobile.injection.module.UiModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AndroidInjectionModule::class,
        ApplicationModule::class,
        UiModule::class,
        PresentationModule::class,
        DataModule::class,
        CacheModule::class,
        RemoteModule::class))
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: GithubTrendingApplication)

}