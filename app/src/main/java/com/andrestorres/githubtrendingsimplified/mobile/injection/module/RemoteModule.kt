package com.andrestorres.githubtrendingsimplified.mobile.injection.module

import com.andrestorres.githubtrendingsimplified.BuildConfig
import com.andrestorres.githubtrendingsimplified.data.repository.ProjectsRemote
import com.andrestorres.githubtrendingsimplified.remote.ProjectsRemoteImpl
import com.andrestorres.githubtrendingsimplified.remote.service.GithubTrendingService
import com.andrestorres.githubtrendingsimplified.remote.service.GithubTrendingServiceFactory
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class RemoteModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideGithubService(): GithubTrendingService {
            return GithubTrendingServiceFactory.makeGithubTrendingService(BuildConfig.DEBUG)
        }
    }

    @Binds
    abstract fun bindProjectsRemote(projectsRemote: ProjectsRemoteImpl): ProjectsRemote
}