package com.andrestorres.githubtrendingsimplified.cache.factory

import com.andrestorres.githubtrendingsimplified.cache.model.CachedProject
import com.andrestorres.githubtrendingsimplified.remote.factory.ProjectFactory
import com.andrestorres.githubtrendingsimplified.domain.model.OwnerModel
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel

object ProjectDataFactory {

    fun makeCachedProject(): CachedProject {
        return CachedProject(DataFactory.randomUuid(),
                DataFactory.randomUuid(), DataFactory.randomUuid(),
                DataFactory.randomInt().toString(), DataFactory.randomUuid(),
                DataFactory.randomUuid(), DataFactory.randomUuid(),
                false)
    }

    fun makeBookmarkedCachedProject(): CachedProject {
        return CachedProject(DataFactory.randomUuid(),
                DataFactory.randomUuid(), DataFactory.randomUuid(),
                DataFactory.randomInt().toString(), DataFactory.randomUuid(),
                DataFactory.randomUuid(), DataFactory.randomUuid(),
                true)
    }

    fun makeOwner(): OwnerModel {
        return OwnerModel(DataFactory.randomUuid(), DataFactory.randomUuid())
    }

    fun makeProjectModel(): ProjectModel {
        return ProjectModel(DataFactory.randomUuid(),
                DataFactory.randomUuid(), DataFactory.randomUuid(),
                DataFactory.randomInt(), DataFactory.randomUuid(),
                DataFactory.randomBoolean(), makeOwner())
    }

    fun makeProjectList(count: Int) : List<ProjectModel> {
        val projects = mutableListOf<ProjectModel>()
        repeat(count) {
            projects.add(ProjectFactory.makeProjectModel())
        }
        return projects
    }
}