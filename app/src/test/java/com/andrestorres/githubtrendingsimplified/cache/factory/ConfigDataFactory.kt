package com.andrestorres.githubtrendingsimplified.cache.factory

import com.andrestorres.githubtrendingsimplified.cache.model.Config


object ConfigDataFactory {

    fun makeCachedConfig(): Config {
        return Config(DataFactory.randomInt(), DataFactory.randomLong())
    }

}