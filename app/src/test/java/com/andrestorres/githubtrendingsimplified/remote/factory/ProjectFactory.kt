package com.andrestorres.githubtrendingsimplified.remote.factory

import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import com.andrestorres.remote.test.factory.OwnerFactory

object ProjectFactory {

    fun makeProjectModel(): ProjectModel {
        return ProjectModel(DataFactory.randomString(),
                DataFactory.randomString(), DataFactory.randomString(),
                DataFactory.randomInt(), DataFactory.randomString(),
                DataFactory.randomBoolean(), OwnerFactory.makeOwnerModel())
    }

    fun makeBookmarkedProjectModel(): ProjectModel {
        return ProjectModel(DataFactory.randomString(),
                DataFactory.randomString(), DataFactory.randomString(),
                DataFactory.randomInt(), DataFactory.randomString(),
                true, OwnerFactory.makeOwnerModel())
    }

    fun makeUnbookmarkedProjectModel(): ProjectModel {
        return ProjectModel(DataFactory.randomString(),
                DataFactory.randomString(), DataFactory.randomString(),
                DataFactory.randomInt(), DataFactory.randomString(),
                false, OwnerFactory.makeOwnerModel())
    }

    fun makeProjectList(count: Int) : List<ProjectModel> {
        val projects = mutableListOf<ProjectModel>()
        repeat(count) {
            projects.add(makeProjectModel())
        }
        return projects
    }
}