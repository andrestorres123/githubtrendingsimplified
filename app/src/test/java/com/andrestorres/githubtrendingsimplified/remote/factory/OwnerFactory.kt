package com.andrestorres.remote.test.factory

import com.andrestorres.githubtrendingsimplified.remote.factory.DataFactory
import com.andrestorres.githubtrendingsimplified.domain.model.OwnerModel

object OwnerFactory {

    fun makeOwnerModel(): OwnerModel {
        return OwnerModel(DataFactory.randomString(), DataFactory.randomString())
    }

}