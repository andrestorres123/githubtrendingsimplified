package com.andrestorres.githubtrendingsimplified.presentation.browse

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.andrestorres.githubtrendingsimplified.domain.actions.bookmark.BookmarkProject
import com.andrestorres.githubtrendingsimplified.domain.actions.bookmark.UnbookmarkProject
import com.andrestorres.githubtrendingsimplified.domain.actions.browse.GetProjects
import com.andrestorres.githubtrendingsimplified.mobile.screens.browse.BrowseProjectsViewModel
import com.andrestorres.githubtrendingsimplified.mobile.state.ResourceState
import com.andrestorres.githubtrendingsimplified.remote.factory.DataFactory
import com.andrestorres.githubtrendingsimplified.remote.factory.ProjectFactory
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import com.nhaarman.mockito_kotlin.*
import io.reactivex.observers.DisposableObserver
import junit.framework.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Captor

@RunWith(JUnit4::class)
class BrowseProjectsViewModelTest {

    @get:Rule var instantTaskExecutorRule = InstantTaskExecutorRule()
    var getProjects = mock<GetProjects>()
    var bookmarkProject = mock<BookmarkProject>()
    var unbookmarkProject = mock<UnbookmarkProject>()
    var projectViewModel = BrowseProjectsViewModel(getProjects,
            bookmarkProject, unbookmarkProject)

    @Captor
    val captor = argumentCaptor<DisposableObserver<List<ProjectModel>>>()

    @Test
    fun fetchProjectsExecutesUseCase() {
        projectViewModel.fetchProjects()

        verify(getProjects, times(1)).execute(any(), eq(null))
    }

    @Test
    fun fetchProjectsReturnsSuccess() {
        val projects = ProjectFactory.makeProjectList(2)

        projectViewModel.fetchProjects()

        verify(getProjects).execute(captor.capture(), eq(null))
        captor.firstValue.onNext(projects)

        assertEquals(ResourceState.SUCCESS,
                projectViewModel.getProjects().value?.status)
    }

    @Test
    fun fetchProjectsReturnsData() {
        val projects = ProjectFactory.makeProjectList(2)

        projectViewModel.fetchProjects()

        verify(getProjects).execute(captor.capture(), eq(null))
        captor.firstValue.onNext(projects)

        assertEquals(projects,
                projectViewModel.getProjects().value?.data)
    }

    @Test
    fun fetchProjectsReturnsError() {
        projectViewModel.fetchProjects()

        verify(getProjects).execute(captor.capture(), eq(null))
        captor.firstValue.onError(RuntimeException())

        assertEquals(ResourceState.ERROR,
                projectViewModel.getProjects().value?.status)
    }

    @Test
    fun fetchProjectsReturnsMessageForError() {
        val errorMessage = DataFactory.randomString()
        projectViewModel.fetchProjects()

        verify(getProjects).execute(captor.capture(), eq(null))
        captor.firstValue.onError(RuntimeException(errorMessage))

        assertEquals(errorMessage,
                projectViewModel.getProjects().value?.message)
    }
}