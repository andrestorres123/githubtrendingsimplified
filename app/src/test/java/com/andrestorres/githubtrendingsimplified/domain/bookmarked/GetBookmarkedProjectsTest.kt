package com.andrestorres.githubtrendingsimplified.domain.bookmarked

import com.andrestorres.githubtrendingsimplified.cache.factory.ProjectDataFactory
import com.andrestorres.githubtrendingsimplified.domain.actions.bookmark.GetBookmarkedProjects
import com.andrestorres.githubtrendingsimplified.domain.executor.PostExecutionThread
import com.andrestorres.githubtrendingsimplified.domain.repository.ProjectsRepository
import com.andrestorres.githubtrendingsimplified.domain.model.ProjectModel
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetBookmarkedProjectsTest {

    private lateinit var getBookmarkedProjects: GetBookmarkedProjects
    @Mock lateinit var projectsRepository: ProjectsRepository
    @Mock lateinit var postExecutionThread: PostExecutionThread

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getBookmarkedProjects = GetBookmarkedProjects(projectsRepository, postExecutionThread)
    }

    @Test
    fun getBookmarkedProjectsCompletes() {
        stubProjectsRepositoryGetBookmarkedProjects(
                Observable.just(ProjectDataFactory.makeProjectList(2)))

        val testObserver = getBookmarkedProjects.buildUseCaseObservable().test()
        testObserver.assertComplete()
    }

    @Test
    fun getBookmarkProjectsCallsRepository() {
        stubProjectsRepositoryGetBookmarkedProjects(
                Observable.just(ProjectDataFactory.makeProjectList(2)))

        getBookmarkedProjects.buildUseCaseObservable().test()
        verify(projectsRepository).getBookmarkedProjects()
    }

    private fun stubProjectsRepositoryGetBookmarkedProjects(single: Observable<List<ProjectModel>>) {
        whenever(projectsRepository.getBookmarkedProjects())
                .thenReturn(single)
    }

}